#include <iostream>
#include <benchmark/benchmark.h>
#include <immintrin.h>
#pragma comment ( lib, "Shlwapi.lib" )

struct PixelColor
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

// 16 pixels at a time
struct PixelsSOA
{
    __m128i arrR;
    __m128i arrG;
    __m128i arrB;
};

enum class SimdOrder
{
    // all arrR, arrG and arrB follow the same order
    Exact,

    // all arrR, arrG and arrB might be arbitrarily ordered
    Arbitrary
};

// 16 pixels at a time
template<SimdOrder T = SimdOrder::Exact>
static inline PixelsSOA AOSToSOA(const PixelColor* pixels)
{
    const auto pixelsPtr = reinterpret_cast<const __m128i*>(pixels);

    // initial read
    //
    // lane1: x0 y0 z0 x1 y1 z1 x2 y2 z2 x3 y3 z3 x4 y4 z4 x5
    // lane2: y5 z5 x6 y6 z6 x7 y7 z7 x8 y8 z8 x9 y9 z9 xA yA
    // lane3: zA xB yB zB xC yC zC xD yD zD xE yE zE xF yF zF
    //
    const auto lane1 = _mm_lddqu_si128(pixelsPtr + 0);
    const auto lane2 = _mm_lddqu_si128(pixelsPtr + 1);
    const auto lane3 = _mm_lddqu_si128(pixelsPtr + 2);

    // collect r0
    //
    // lane1: x0  -  - x1  -  - x2  -  - x3  -  - x4  -  - x5
    // lane2:  -  - x6  -  - x7  -  - x8  -  - x9  -  - xA  -
    // lane3:  - xB  -  - xC  -  - xD  -  - xE  -  - xF  -  -
    //
    // becomes
    //    r0: x0  - x6 x1  - x7 x2  - x8 x3  - x9 x4  - xA x5
    // lane3:  - xB  -  - xC  -  - xD  -  - xE  -  - xF  -  -
    //
    // and then
    //    r0: x0 xB x6 x1 xC x7 x2 xD x8 x3 xE x9 x4 xF xA x5
    //
    auto r0 = _mm_blendv_epi8(lane1, lane2, _mm_setr_epi8(0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0));
    r0 = _mm_blendv_epi8(r0, lane3, _mm_setr_epi8(0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0));

    // collect g0
    //
    // lane1:  - y0  -  - y1  -  - y2  -  - y3  -  - y4  -  -
    // lane2: y5  -  - y6  -  - y7  -  - y8  -  - y9  -  - yA
    // lane3:  -  - yB  -  - yC  -  - yD  -  - yE  -  - yF  -
    //
    // becomes
    //    g0: y5 y0  - y6 y1  - y7 y2  - y8 y3  - y9 y4  - yA
    // lane3:  -  - yB  -  - yC  -  - yD  -  - yE  -  - yF  -
    //
    // and then
    //    g0: y5 y0 yB y6 y1 yC y7 y2 yD y8 y3 yE y9 y4 yF yA
    //
    auto g0 = _mm_blendv_epi8(lane1, lane2, _mm_setr_epi8(-1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1));
    g0 = _mm_blendv_epi8(g0, lane3, _mm_setr_epi8(0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0));

    // collect b0
    //
    // lane1:  -  - z0  -  - z1  -  - z2  -  - z3  -  - z4  -
    // lane2:  - z5  -  - z6  -  - z7  -  - z8  -  - z9  -  -
    // lane3: zA  -  - zB  -  - zC  -  - zD  -  - zE  -  - zF
    //
    // becomes
    //    b0:  - z5 z0  - z6 z1  - z7 z2  - z8 z3  - z9 z4  -
    // lane3: zA  -  - zB  -  - zC  -  - zD  -  - zE  -  - zF
    //
    // and then
    //    b0: zA z5 z0 zB z6 z1 zC z7 z2 zD z8 z3 zE z9 z4 zF
    //
    auto b0 = _mm_blendv_epi8(lane1, lane2, _mm_setr_epi8(0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0));
    b0 = _mm_blendv_epi8(b0, lane3, _mm_setr_epi8(-1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1));

    if constexpr (T == SimdOrder::Exact)
    {
        // x0 xB x6 x1 xC x7 x2 xD x8 x3 xE x9 x4 xF xA x5 -> x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 xA xB xC xD xE xF
        r0 = _mm_shuffle_epi8(r0, _mm_setr_epi8(0, 3, 6, 9, 12, 15, 2, 5, 8, 11, 14, 1, 4, 7, 10, 13));

        // y5 y0 yB y6 y1 yC y7 y2 yD y8 y3 yE y9 y4 yF yA -> y0 y1 y2 y3 y4 y5 y6 y7 y8 y9 yA yB yC yD yE yF
        g0 = _mm_shuffle_epi8(g0, _mm_setr_epi8(1, 4, 7, 10, 13, 0, 3, 6, 9, 12, 15, 2, 5, 8, 11, 14));

        // zA z5 z0 zB z6 z1 zC z7 z2 zD z8 z3 zE z9 z4 zF -> z0 z1 z2 z3 z4 z5 z6 z7 z8 z9 zA zB zC zD zE zF
        b0 = _mm_shuffle_epi8(b0, _mm_setr_epi8(2, 5, 8, 11, 14, 1, 4, 7, 10, 13, 0, 3, 6, 9, 12, 15));
    }

    return { r0, g0, b0 };
}

// 16 pixels at a time
static inline void SOAToAOS(PixelColor* pixels, const __m128i& arrR, const __m128i& arrG, const __m128i& arrB)
{
    //Transform lanes
    // x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 xA xB xC xD xE xF -> x0 xB x6 x1 xC x7 x2 xD x8 x3 xE x9 x4 xF xA x5
    const auto lane1 = _mm_shuffle_epi8(arrR, _mm_setr_epi8(0, 11, 6, 1, 12, 7, 2, 13, 8, 3, 14, 9, 4, 15, 10, 5));
    // y0 y1 y2 y3 y4 y5 y6 y7 y8 y9 yA yB yC yD yE yF -> y5 y0 yB y6 y1 yC y7 y2 yD y8 y3 yE y9 y4 yF yA
    const auto lane2 = _mm_shuffle_epi8(arrG, _mm_setr_epi8(5, 0, 11, 6, 1, 12, 7, 2, 13, 8, 3, 14, 9, 4, 15, 10));
    // z0 z1 z2 z3 z4 z5 z6 z7 z8 z9 zA zB zC zD zE zF -> zA z5 z0 zB z6 z1 zC z7 z2 zD z8 z3 zE z9 z4 zF
    const auto lane3 = _mm_shuffle_epi8(arrB, _mm_setr_epi8(10, 5, 0, 11, 6, 1, 12, 7, 2, 13, 8, 3, 14, 9, 4, 15));

    // collect pixs0
    //
    // lane1: x0  -  - x1  -  - x2  -  - x3  -  - x4  -  - x5
    // lane2:  - y0  -  - y1  -  - y2  -  - y3  -  - y4  -  -
    // lane3:  -  - z0  -  - z1  -  - z2  -  - z3  -  - z4  -
    //
    // becomes
    // pixs0: x0 y0  - x1 y1  - x2 y2  - x3 y3  - x4 y4  - x5
    // lane3:  -  - z0  -  - z1  -  - z2  -  - z3  -  - z4  -
    //
    // and then
    // pixs0: x0 y0 z0 x1 y1 z1 x2 y2 z2 x3 y3 z3 x4 y4 z4 x5
    //
    auto pixs0 = _mm_blendv_epi8(lane1, lane2, _mm_setr_epi8(0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0));
    pixs0 = _mm_blendv_epi8(pixs0, lane3, _mm_setr_epi8(0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0));

    // collect pixs1
    //
    // lane1:  -  - x6  -  - x7  -  - x8  -  - x9  -  - xA  -
    // lane2: y5  -  - y6  -  - y7  -  - y8  -  - y9  -  - yA
    // lane3:  - z5  -  - z6  -  - z7  -  - z8  -  - z9  -  -
    //
    // becomes
    // pixs1: y5  - x6 y6  - x7 y7  - x8 y8  - x9 y9  - xA yA
    // lane3:  - z5  -  - z6  -  - z7  -  - z8  -  - z9  -
    //
    // and then
    // pixs1: y5 z5 x6 y6 z6 x7 y7 z7 x8 y8 z8 x9 y9 z9 xA yA
    //
    auto pixs1 = _mm_blendv_epi8(lane1, lane2, _mm_setr_epi8(-1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1));
    pixs1 = _mm_blendv_epi8(pixs1, lane3, _mm_setr_epi8(0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0));

    // collect segs2
    //
    // lane1:  - xB  -  - xC  -  - xD  -  - xE  -  - xF  -  -
    // lane2:  -  - yB  -  - yC  -  - yD  -  - yE  -  - yF  -
    // lane3: zA  -  - zB  -  - zC  -  - zD  -  - zE  -  - zF
    //
    // becomes
    // segs2:  - xB yB  - xC yC  - xD yD  - xE yE  - xF yF  -
    // lane3: zA  -  - zB  -  - zC  -  - zD  -  - zE  -  - zF
    //
    // and then
    // segs2: zA xB yB zB xC yC zC xD yD zD xE yE zE xF yF zF
    //
    auto pixs2 = _mm_blendv_epi8(lane1, lane2, _mm_setr_epi8(0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0));
    pixs2 = _mm_blendv_epi8(pixs2, lane3, _mm_setr_epi8(-1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1));

    const auto pixelsData = reinterpret_cast<uint8_t*>(pixels);
    _mm_storeu_epi8(pixelsData + 16 * 0, pixs0);
    _mm_storeu_epi8(pixelsData + 16 * 1, pixs1);
    _mm_storeu_epi8(pixelsData + 16 * 2, pixs2);
}

static inline void SOAToAOS(
    PixelColor* pixels,
    const uint8_t* arrR,
    const uint8_t* arrG,
    const uint8_t* arrB)
{
    const auto lane1 = _mm_lddqu_si128(reinterpret_cast<const __m128i*>(arrR));
    const auto lane2 = _mm_lddqu_si128(reinterpret_cast<const __m128i*>(arrG));
    const auto lane3 = _mm_lddqu_si128(reinterpret_cast<const __m128i*>(arrB));
    SOAToAOS(pixels, lane1, lane2, lane3);
}


static bool CompareImages(const uint8_t* gradA, const uint8_t* gradB, int32_t width, int32_t height)
{
    for (int row = 1; row < height - 1; ++row)
    {
        for (int col = 1; col < width - 1; ++col)
        {
            const auto pos = row * width + col;
            if (gradA[pos] != gradB[pos])
            {
                return false;
            }
        }
    }
    return true;
}

constexpr auto fullSize = size_t{ 6000 };

void InitialFillPixelVector(std::vector<PixelColor>& vec)
{
    srand(42);
    for (auto& it : vec)
    {
        it = { static_cast<uint8_t>(rand() % 256), static_cast<uint8_t>(rand() % 256), static_cast<uint8_t>(rand() % 256) };
    }
}

static void ScalarToSOA(const std::vector<PixelColor>& vec, std::vector<uint8_t>& arrR, std::vector<uint8_t>& arrG, std::vector<uint8_t>& arrB)
{
    for (auto i = size_t{ 0 }; i < fullSize; i++)
    {
        arrR[i] = vec[i].r;
        arrG[i] = vec[i].g;
        arrB[i] = vec[i].b;
    }
}

static void SIMDToSOA(const std::vector<PixelColor>& vec, std::vector<uint8_t>& arrR, std::vector<uint8_t>& arrG, std::vector<uint8_t>& arrB)
{
    auto i = size_t{ 0 };
    const auto vecPtr = vec.data();
    const auto arrRPtr = arrR.data();
    const auto arrGPtr = arrG.data();
    const auto arrBPtr = arrB.data();
    for (; i + 15 < fullSize; i += 16)
    {
        const auto& [mmarrR, mmarrG, mmarrB] = AOSToSOA(vecPtr + i);
        _mm_storeu_si128(reinterpret_cast<__m128i*>(arrRPtr + i), mmarrR);
        _mm_storeu_si128(reinterpret_cast<__m128i*>(arrGPtr + i), mmarrG);
        _mm_storeu_si128(reinterpret_cast<__m128i*>(arrBPtr + i), mmarrB);
    }

    for (; i < fullSize; i++)
    {
        arrR[i] = vec[i].r;
        arrG[i] = vec[i].g;
        arrB[i] = vec[i].b;
    }
}

static void SOAToScalar(std::vector<PixelColor>& vec, const std::vector<uint8_t>& arrR, const std::vector<uint8_t>& arrG, const std::vector<uint8_t>& arrB)
{
    for (auto i = size_t{ 0 }; i < fullSize; i++)
    {
        vec[i].r = arrR[i];
        vec[i].g = arrG[i];
        vec[i].b = arrB[i];
    }
}

static void SOAToSIMD(std::vector<PixelColor>& vec, const std::vector<uint8_t>& arrR, const std::vector<uint8_t>& arrG, const std::vector<uint8_t>& arrB)
{
    auto i = size_t{ 0 };
    const auto vecPtr = vec.data();
    const auto arrRPtr = arrR.data();
    const auto arrGPtr = arrG.data();
    const auto arrBPtr = arrB.data();
    for (; i + 15 < fullSize; i += 16)
    {
        SOAToAOS(vecPtr + i, arrRPtr + i, arrGPtr + i, arrBPtr + i);
    }

    for (; i < fullSize; i++)
    {
        vec[i].r = arrR[i];
        vec[i].g = arrG[i];
        vec[i].b = arrB[i];
    }
}

int main(int argc, char** argv)
{
    std::vector<PixelColor> region(fullSize);
    std::vector<PixelColor> regionSIMD(fullSize);
    InitialFillPixelVector(region);

    std::vector<uint8_t> arrR(fullSize);
    std::vector<uint8_t> arrG(fullSize);
    std::vector<uint8_t> arrB(fullSize);

    std::vector<uint8_t> arrRSIMD(fullSize);
    std::vector<uint8_t> arrGSIMD(fullSize);
    std::vector<uint8_t> arrBSIMD(fullSize);

    ScalarToSOA(region, arrR, arrG, arrB);
    SIMDToSOA(region, arrRSIMD, arrGSIMD, arrBSIMD);
    SOAToSIMD(regionSIMD, arrR, arrG, arrB);

    const auto isEqual = std::equal(arrR.begin(), arrR.end(), arrRSIMD.begin(), arrRSIMD.end())
        && std::equal(arrG.begin(), arrG.end(), arrGSIMD.begin(), arrGSIMD.end())
        && std::equal(arrB.begin(), arrB.end(), arrBSIMD.begin(), arrBSIMD.end())
        && std::equal(region.begin(), region.end(), regionSIMD.begin(), regionSIMD.end(), [](const auto& a, const auto& b)
            {
                const auto isEqual = (a.r == b.r) && (a.g == b.g) && (a.b == b.b);
                return isEqual;
            });

    if (isEqual)
    {
        std::cout << "Data Correct!" << std::endl;
    }
    else
    {
        std::cout << "Data is not equal!" << std::endl;
        return 1;
    }

    ::benchmark::Initialize(&argc, argv);

    if (::benchmark::ReportUnrecognizedArguments(argc, argv))
    {
        return 1;
    }

    ::benchmark::RunSpecifiedBenchmarks();
}

static void ScalarAOSToSOA(benchmark::State& state)
{
    std::vector<PixelColor> region(fullSize);
    InitialFillPixelVector(region);

    std::vector<uint8_t> arrR(fullSize);
    std::vector<uint8_t> arrG(fullSize);
    std::vector<uint8_t> arrB(fullSize);

    for ([[maybe_unused]] auto _state : state)
    {
        ScalarToSOA(region, arrR, arrG, arrB);
    }
}
BENCHMARK(ScalarAOSToSOA)->Unit(benchmark::TimeUnit::kMicrosecond)->Repetitions(5)->MinTime(2);

static void SIMDAOSToSOA(benchmark::State& state)
{
    std::vector<PixelColor> region(fullSize);
    InitialFillPixelVector(region);

    std::vector<uint8_t> arrR(fullSize);
    std::vector<uint8_t> arrG(fullSize);
    std::vector<uint8_t> arrB(fullSize);

    for ([[maybe_unused]] auto _state : state)
    {
        SIMDToSOA(region, arrR, arrG, arrB);
    }
}
BENCHMARK(SIMDAOSToSOA)->Unit(benchmark::TimeUnit::kMicrosecond)->Repetitions(5)->MinTime(2);


static void ScalarSOAToAOS(benchmark::State& state)
{
    std::vector<PixelColor> region(fullSize);
    InitialFillPixelVector(region);

    std::vector<uint8_t> arrR(fullSize);
    std::vector<uint8_t> arrG(fullSize);
    std::vector<uint8_t> arrB(fullSize);
    ScalarToSOA(region, arrR, arrG, arrB);
    region.clear();


    for ([[maybe_unused]] auto _state : state)
    {
        SOAToScalar(region, arrR, arrG, arrB);
    }
}
BENCHMARK(ScalarSOAToAOS)->Unit(benchmark::TimeUnit::kMicrosecond)->Repetitions(5)->MinTime(2);

static void SIMDSOAToAOS(benchmark::State& state)
{
    std::vector<PixelColor> region(fullSize);
    InitialFillPixelVector(region);

    std::vector<uint8_t> arrR(fullSize);
    std::vector<uint8_t> arrG(fullSize);
    std::vector<uint8_t> arrB(fullSize);
    ScalarToSOA(region, arrR, arrG, arrB);
    region.clear();


    for ([[maybe_unused]] auto _state : state)
    {
        SOAToSIMD(region, arrR, arrG, arrB);
    }
}
BENCHMARK(SIMDSOAToAOS)->Unit(benchmark::TimeUnit::kMicrosecond)->Repetitions(5)->MinTime(2);

