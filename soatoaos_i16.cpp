#include <iostream>
#include <benchmark/benchmark.h>
#include <immintrin.h>
#pragma comment ( lib, "Shlwapi.lib" )

struct Segment
{
    int16_t row;
    int16_t colStart;
    int16_t colStop;
};

   // 8 segments at a time
struct SegmentsSOA
{
    __m128i rows;
    __m128i colsStart;
    __m128i colsStop;
};

enum class SimdOrder
{
    // all rows, colStarts and colStops follow the same order
    Exact,

    // all rows, colStarts and colStops might be arbitrarily ordered
    Arbitrary
};

// 8 segments at a time
template<SimdOrder T = SimdOrder::Exact>
static inline SegmentsSOA AOSToSOA(const Segment* segments)
{
    const auto segmentsPtr = reinterpret_cast<const __m128i*>(segments);

    // initial read
    //
    // lane1: x0 y0 z0 x1 y1 z1 x2 y2
    // lane2: z2 x3 y3 z3 x4 y4 z4 x5
    // lane3: y5 z5 x6 y6 z6 x7 y7 z7
    //
    const auto lane1 = _mm_lddqu_si128(segmentsPtr + 0);
    const auto lane2 = _mm_lddqu_si128(segmentsPtr + 1);
    const auto lane3 = _mm_lddqu_si128(segmentsPtr + 2);

    // collect rows0
    //
    // lane1: x0  -  - x1  -  - x2  -
    // lane2:  - x3  -  - x4 -  -  x5
    // lane3:  -  -  x6 -  -  x7 -  -
    //
    // becomes
    // rows0: x0 x3  - x1 x4  - x2 x5
    // lane3:  -  -  x6 -  -  x7 -  -
    //
    // and then
    // rows0: x0 x3 x6 x1 x4 x7 x2 x5
    //
    auto rows0 = _mm_blend_epi16(lane1, lane2, (1 << 1) + (1 << 4) + (1 << 7));
    rows0 = _mm_blend_epi16(rows0, lane3, (1 << 2) + (1 << 5));

    // collect cols1
    //
    // lane1:  - y0  -  - y1  -  - y2
    // lane2:  -  - y3  -  - y4  -  -
    // lane3: y5  -  - y6  -  - y7  -
    //
    // becomes
    // cols1:  - y0 y3  - y1 y4  - y2
    // lane3: y5  -  - y6  -  - y7  -
    //
    // and then
    // cols1: y5 y0 y3 y6 y1 y4 y7 y2
    //
    auto cols1 = _mm_blend_epi16(lane1, lane2, (1 << 2) + (1 << 5));
    cols1 = _mm_blend_epi16(cols1, lane3, (1 << 0) + (1 << 3) + (1 << 6));

    // collect cols2
    //
    // lane1:  -  - z0  -  - z1  -  -
    // lane2: z2  -  - z3  -  - z4  -
    // lane3:  - z5  -  - z6  -  - z7
    //
    // becomes
    // cols2: z2  - z0 z3  - z1 z4  -
    // lane3:  - z5  -  - z6  -  - z7
    //
    // and then
    // cols2: z2 z5 z0 z3 z6 z1 z4 z7
    //
    auto cols2 = _mm_blend_epi16(lane1, lane2, (1 << 0) + (1 << 3) + (1 << 6));
    cols2 = _mm_blend_epi16(cols2, lane3, (1 << 1) + (1 << 4) + (1 << 7));

    if constexpr (T == SimdOrder::Exact)
    {
#define POS(i) 2 * i, 2 * i + 1

        // x0 x3 x6 x1 x4 x7 x2 x5 -> x0 x1 x2 x3 x4 x5 x6 x7
        rows0 = _mm_shuffle_epi8(rows0, _mm_setr_epi8(POS(0), POS(3), POS(6), POS(1), POS(4), POS(7), POS(2), POS(5)));

        // y5 y0 y3 y6 y1 y4 y7 y2 -> y0 y1 y2 y3 y4 y5 y6 y7
        cols1 = _mm_shuffle_epi8(cols1, _mm_setr_epi8(POS(1), POS(4), POS(7), POS(2), POS(5), POS(0), POS(3), POS(6)));

        // z2 z5 z0 z3 z6 z1 z4 z7 -> z0 z1 z2 z3 z4 z5 z6 z7
        cols2 = _mm_shuffle_epi8(cols2, _mm_setr_epi8(POS(2), POS(5), POS(0), POS(3), POS(6), POS(1), POS(4), POS(7)));

#undef POS
    }

    return { rows0, cols1, cols2 };
}

// 8 segments at a time
static inline void SOAToAOS(Segment* segments, const __m128i& rows, const __m128i& colStarts, const __m128i& colStops)
{
#define POS(i) 2 * i, 2 * i + 1
    //Transform lanes
    // x0 x1 x2 x3 x4 x5 x6 x7 -> x0 x3 x6 x1 x4 x7 x2 x5
    const auto lane1 = _mm_shuffle_epi8(rows, _mm_setr_epi8(POS(0), POS(3), POS(6), POS(1), POS(4), POS(7), POS(2), POS(5)));
    // y0 y1 y2 y3 y4 y5 y6 y7 -> y5 y0 y3 y6 y1 y4 y7 y2
    const auto lane2 = _mm_shuffle_epi8(colStarts, _mm_setr_epi8(POS(5), POS(0), POS(3), POS(6), POS(1), POS(4), POS(7), POS(2)));
    // z0 z1 z2 z3 z4 z5 z6 z7 -> z2 z5 z0 z3 z6 z1 z4 z7
    const auto lane3 = _mm_shuffle_epi8(colStops, _mm_setr_epi8(POS(2), POS(5), POS(0), POS(3), POS(6), POS(1), POS(4), POS(7)));
#undef POS

    // collect segs0
    //
    // lane1: x0  -  - x1  -  - x2  -
    // lane2:  - y0  -  - y1  -  - y2
    // lane3:  -  - z0  -  - z1  -  -
    //
    // becomes
    // segs0: x0 y0  - x1 y1  - x2 y2
    // lane3:  -  - z0  -  - z1  -  -
    //
    // and then
    // segs0: x0 y0 z0 x1 y1 z1 x2 y2
    //
    auto segs0 = _mm_blend_epi16(lane1, lane2, (1 << 1) + (1 << 4) + (1 << 7));
    segs0 = _mm_blend_epi16(segs0, lane3, (1 << 2) + (1 << 5));

    // collect segs1
    //
    // lane1:  - x3  -  - x4  -  - x5
    // lane2:  -  - y3  -  - y4  -  -
    // lane3: z2  -  - z3  -  - z4  -
    //
    // becomes
    // segs1:  - x3 y3  - x4 y4  - x5
    // lane3: z2  -  - z3  -  - z4  -
    //
    // and then
    // segs1: z2 x3 y3 z3 x4 y4 z4 x5
    //
    auto segs1 = _mm_blend_epi16(lane1, lane2, (1 << 2) + (1 << 5));
    segs1 = _mm_blend_epi16(segs1, lane3, (1 << 0) + (1 << 3) + (1 << 6));

    // collect segs2
    //
    // lane1:  -  - x6  -  - x7  -  -
    // lane2: y5  -  - y6  -  - y7  -
    // lane3:  - z5  -  - z6  -  - z7
    //
    // becomes
    // segs2: y5  - x6 y6  - x7 y7  -
    // lane3:  - z5  -  - z6  -  - z7
    //
    // and then
    // segs2: y5 z5 x6 y6 z6 x7 y7 z7
    //
    auto segs2 = _mm_blend_epi16(lane1, lane2, (1 << 0) + (1 << 3) + (1 << 6));
    segs2 = _mm_blend_epi16(segs2, lane3, (1 << 1) + (1 << 4) + (1 << 7));

    const auto segmentsData = reinterpret_cast<int16_t*>(segments);
    _mm_storeu_epi16(segmentsData + 8 * 0, segs0);
    _mm_storeu_epi16(segmentsData + 8 * 1, segs1);
    _mm_storeu_epi16(segmentsData + 8 * 2, segs2);
}

static inline void SOAToAOS(
    Segment* segments,
    const int16_t* rows,
    const int16_t* colStarts,
    const int16_t* colStops)
{
    const auto lane1 = _mm_lddqu_si128(reinterpret_cast<const __m128i*>(rows));
    const auto lane2 = _mm_lddqu_si128(reinterpret_cast<const __m128i*>(colStarts));
    const auto lane3 = _mm_lddqu_si128(reinterpret_cast<const __m128i*>(colStops));
    SOAToAOS(segments, lane1, lane2, lane3);
}


static bool CompareImages(const int16_t* gradA, const int16_t* gradB, int32_t width, int32_t height)
{
    for (int row = 1; row < height - 1; ++row)
    {
        for (int col = 1; col < width - 1; ++col)
        {
            const auto pos = row * width + col;
            if (gradA[pos] != gradB[pos])
            {
                return false;
            }
        }
    }
    return true;
}

constexpr auto fullSize = size_t{ 6000 };

void InitialFillSegmentVector(std::vector<Segment>& vec)
{
    srand(42);
    auto row = int16_t{ 0 };
    for (auto& it : vec)
    {
        it = { row++, static_cast<int16_t>(rand() % 50), static_cast<int16_t>(rand() % 800 + 50) };
    }
}

static void ScalarToSOA(const std::vector<Segment>& vec, std::vector<int16_t>& rows, std::vector<int16_t>& colStarts, std::vector<int16_t>& colStops)
{
    for (auto i = size_t{ 0 }; i < fullSize; i++)
    {
        rows[i] = vec[i].row;
        colStarts[i] = vec[i].colStart;
        colStops[i] = vec[i].colStop;
    }
}

static void SIMDToSOA(const std::vector<Segment>& vec, std::vector<int16_t>& rows, std::vector<int16_t>& colStarts, std::vector<int16_t>& colStops)
{
    auto i = size_t{ 0 };
    const auto vecPtr = vec.data();
    const auto rowsPtr = rows.data();
    const auto colStartsPtr = colStarts.data();
    const auto colStopsPtr = colStops.data();
    for (; i + 7 < fullSize; i += 8)
    {
        const auto& [mmRows, mmColStarts, mmColStops] = AOSToSOA(vecPtr + i);
        _mm_storeu_si128(reinterpret_cast<__m128i*>(rowsPtr + i), mmRows);
        _mm_storeu_si128(reinterpret_cast<__m128i*>(colStartsPtr + i), mmColStarts);
        _mm_storeu_si128(reinterpret_cast<__m128i*>(colStopsPtr + i), mmColStops);
    }

    for (; i < fullSize; i++)
    {
        rows[i] = vec[i].row;
        colStarts[i] = vec[i].colStart;
        colStops[i] = vec[i].colStop;
    }
}

static void SOAToScalar(std::vector<Segment>& vec, const std::vector<int16_t>& rows, const std::vector<int16_t>& colStarts, const std::vector<int16_t>& colStops)
{
    for (auto i = size_t{ 0 }; i < fullSize; i++)
    {
        vec[i].row = rows[i];
        vec[i].colStart = colStarts[i];
        vec[i].colStop = colStops[i];
    }
}

static void SOAToSIMD(std::vector<Segment>& vec, const std::vector<int16_t>& rows, const std::vector<int16_t>& colStarts, const std::vector<int16_t>& colStops)
{
    auto i = size_t{ 0 };
    const auto vecPtr = vec.data();
    const auto rowsPtr = rows.data();
    const auto colStartsPtr = colStarts.data();
    const auto colStopsPtr = colStops.data();
    for (; i + 7 < fullSize; i += 8)
    {
        SOAToAOS(vecPtr + i, rowsPtr + i, colStartsPtr + i, colStopsPtr + i);
    }

    for (; i < fullSize; i++)
    {
        vec[i].row = rows[i];
        vec[i].colStart = colStarts[i];
        vec[i].colStop = colStops[i];
    }
}

int main(int argc, char** argv)
{
    std::vector<Segment> region(fullSize);
    std::vector<Segment> regionSIMD(fullSize);
    InitialFillSegmentVector(region);

    std::vector<int16_t> rows(fullSize);
    std::vector<int16_t> colStarts(fullSize);
    std::vector<int16_t> colStops(fullSize);

    std::vector<int16_t> rowsSIMD(fullSize);
    std::vector<int16_t> colStartsSIMD(fullSize);
    std::vector<int16_t> colStopsSIMD(fullSize);
    
    ScalarToSOA(region, rows, colStarts, colStops);
    SIMDToSOA(region, rowsSIMD, colStartsSIMD, colStopsSIMD);
    SOAToSIMD(regionSIMD, rows, colStarts, colStops);
    
    const auto isEqual = std::equal(rows.begin(), rows.end(), rowsSIMD.begin(), rowsSIMD.end())
        && std::equal(colStarts.begin(), colStarts.end(), colStartsSIMD.begin(), colStartsSIMD.end())
        && std::equal(colStops.begin(), colStops.end(), colStopsSIMD.begin(), colStopsSIMD.end())
        && std::equal(region.begin(), region.end(), regionSIMD.begin(), regionSIMD.end(), [](const auto& a, const auto& b)
        {
            const auto isEqual = (a.row == b.row) && (a.colStart == b.colStart) && (a.colStop == b.colStop);
            return isEqual;
        });

    if (isEqual)
    {
        std::cout << "Data Correct!" << std::endl;
    }
    else
    {
        std::cout << "Data is not equal!" << std::endl;
        return 1;
    }

    ::benchmark::Initialize(&argc, argv);

    if (::benchmark::ReportUnrecognizedArguments(argc, argv))
    {
        return 1;
    }

    ::benchmark::RunSpecifiedBenchmarks();
}

static void ScalarAOSToSOA(benchmark::State& state)
{
    std::vector<Segment> region(fullSize);
    InitialFillSegmentVector(region);

    std::vector<int16_t> rows(fullSize);
    std::vector<int16_t> colStarts(fullSize);
    std::vector<int16_t> colStops(fullSize);

    for ([[maybe_unused]] auto _state : state)
    {
        ScalarToSOA(region, rows, colStarts, colStops);
    }
}
BENCHMARK(ScalarAOSToSOA)->Unit(benchmark::TimeUnit::kMicrosecond)->Repetitions(5)->MinTime(2);

static void SIMDAOSToSOA(benchmark::State& state)
{
    std::vector<Segment> region(fullSize);
    InitialFillSegmentVector(region);

    std::vector<int16_t> rows(fullSize);
    std::vector<int16_t> colStarts(fullSize);
    std::vector<int16_t> colStops(fullSize);

    for ([[maybe_unused]] auto _state : state)
    {
        SIMDToSOA(region, rows, colStarts, colStops);
    }
}
BENCHMARK(SIMDAOSToSOA)->Unit(benchmark::TimeUnit::kMicrosecond)->Repetitions(5)->MinTime(2);


static void ScalarSOAToAOS(benchmark::State& state)
{
    std::vector<Segment> region(fullSize);
    InitialFillSegmentVector(region);

    std::vector<int16_t> rows(fullSize);
    std::vector<int16_t> colStarts(fullSize);
    std::vector<int16_t> colStops(fullSize);
    ScalarToSOA(region, rows, colStarts, colStops);
    region.clear();


    for ([[maybe_unused]] auto _state : state)
    {
        SOAToScalar(region, rows, colStarts, colStops);
    }
}
BENCHMARK(ScalarSOAToAOS)->Unit(benchmark::TimeUnit::kMicrosecond)->Repetitions(5)->MinTime(2);

static void SIMDSOAToAOS(benchmark::State& state)
{
    std::vector<Segment> region(fullSize);
    InitialFillSegmentVector(region);

    std::vector<int16_t> rows(fullSize);
    std::vector<int16_t> colStarts(fullSize);
    std::vector<int16_t> colStops(fullSize);
    ScalarToSOA(region, rows, colStarts, colStops);
    region.clear();


    for ([[maybe_unused]] auto _state : state)
    {
        SOAToSIMD(region, rows, colStarts, colStops);
    }
}
BENCHMARK(SIMDSOAToAOS)->Unit(benchmark::TimeUnit::kMicrosecond)->Repetitions(5)->MinTime(2);

